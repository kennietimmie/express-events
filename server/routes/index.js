
const EventController = require('../controllers').events


module.exports = (app) => {

	app.post('/api/events', EventController.create); // 01
	app.get('/api/events/actors/:id', EventController.retrieveEventsByActorId); // 02

	app.get('/api/actors/streak', EventController.streak); // 03
	app.put('/api/actors', EventController.updateActor); // 04

	app.get('/api/events', EventController.list); // 05
	app.get('/api/actors', EventController.retrieve); // 06

	app.delete('/api/erase', EventController.erase); // 07


}