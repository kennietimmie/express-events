'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Event extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Event.init({
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      unique: true
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    actor: {
      type: DataTypes.JSONB,
      allowNull: false,
      validate: {
        missingKey: function () {
          if (!Object.keys(this.actor).every(i => ["id", "login", "avatar_url"].includes(i))) {
            throw new Error('Missing actor key(s)');
          }
          return;
        }
      }
    },
    repo: {
      type: DataTypes.JSONB,
      allowNull: false,
      validate: {
        missingKey: function () {
          if (!Object.keys(this.repo).every(i => ["id", "name", "url"].includes(i))) {
            throw new Error('Missing repo key(s)');
          }
          return;
        }
      }
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'Event',
  });
  return Event;
};