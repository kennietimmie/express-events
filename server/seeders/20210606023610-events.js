'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

     await queryInterface.bulkInsert('Events', [
       // eric66 2907782
       {
         "id": 2712153979,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 2907782,
           "login": "eric66",
           "avatar_url": "https://avatars.com/2907782"
         }),
         "repo": JSON.stringify({
           "id": 426482,
           "name": "pestrada/voluptatem",
           "url": "https://github.com/pestrada/voluptatem"
         }),
         "created_at": "2014-07-13 08:13:31"
       },
       {
         "id": 271253979,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 2907782,
           "login": "eric66",
           "avatar_url": "https://avatars.com/2907782"
         }),
         "repo": JSON.stringify({
           "id": 426482,
           "name": "pestrada/voluptatem",
           "url": "https://github.com/pestrada/voluptatem"
         }),
         "created_at": "2014-07-13 08:13:31"
       },

       //iholloway 4276597
       {
         "id": 4633249595,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 4276597,
           "login": "iholloway",
           "avatar_url": "https://avatars.com/4276597"
         }),
         "repo": JSON.stringify({
           "id": 269910,
           "name": "iholloway/aperiam-consectetur",
           "url": "https://github.com/iholloway/aperiam-consectetur"
         }),
         "created_at": "2016-04-18 00:13:31"
       },
       {
         "id": 463349595,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 4276597,
           "login": "iholloway",
           "avatar_url": "https://avatars.com/4276597"
         }),
         "repo": JSON.stringify({
           "id": 269910,
           "name": "iholloway/aperiam-consectetur",
           "url": "https://github.com/iholloway/aperiam-consectetur"
         }),
         "created_at": "2016-04-18 00:13:31"
       },

       // danile51 3698252
       {
         "id": 1514531484,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 3698252,
           "login": "daniel51",
           "avatar_url": "https://avatars.com/3698252"
         }),
         "repo": JSON.stringify({
           "id": 451024,
           "name": "daniel51/quo-tempore-dolor",
           "url": "https://github.com/daniel51/quo-tempore-dolor"
         }),
         "created_at": "2013-06-16 02:13:31"
       },
       {
         "id": 151431484,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 3698252,
           "login": "daniel51",
           "avatar_url": "https://avatars.com/3698252"
         }),
         "repo": JSON.stringify({
           "id": 451024,
           "name": "daniel51/quo-tempore-dolor",
           "url": "https://github.com/daniel51/quo-tempore-dolor"
         }),
         "created_at": "2013-06-16 02:13:31"
       },

       // oscar 2917996
       {
         "id": 4501280090,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 2917996,
           "login": "oscarschmidt",
           "avatar_url": "https://avatars.com/2917996"
         }),
         "repo": JSON.stringify({
           "id": 301227,
           "name": "oscarschmidt/doloremque-expedita",
           "url": "https://github.com/oscarschmidt/doloremque-expedita"
         }),
         "created_at": "2016-03-05 10:13:31"
       },
       {
         "id": 450180090,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 2917996,
           "login": "oscarschmidt",
           "avatar_url": "https://avatars.com/2917996"
         }),
         "repo": JSON.stringify({
           "id": 301227,
           "name": "oscarschmidt/doloremque-expedita",
           "url": "https://github.com/oscarschmidt/doloremque-expedita"
         }),
         "created_at": "2016-03-05 10:13:31"
       },

       // xnguyen 2222918
       {
         "id": 3822562012,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 2222918,
           "login": "xnguyen",
           "avatar_url": "https://avatars.com/2222918"
         }),
         "repo": JSON.stringify({
           "id": 425512,
           "name": "cohenjacqueline/quam-autem-suscipit",
           "url": "https://github.com/cohenjacqueline/quam-autem-suscipit"
         }),
         "created_at": "2015-07-15 15:13:31"
       },
       {
         "id": 382252012,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 2222918,
           "login": "xnguyen",
           "avatar_url": "https://avatars.com/2222918"
         }),
         "repo": JSON.stringify({
           "id": 425512,
           "name": "cohenjacqueline/quam-autem-suscipit",
           "url": "https://github.com/cohenjacqueline/quam-autem-suscipit"
         }),
         "created_at": "2015-07-15 15:13:31"
       },

       {
         "id": 1319379787,
         "type": "PushEvent",
         "actor": JSON.stringify({
           "id": 3466404,
           "login": "khunt",
           "avatar_url": "https://avatars.com/3466404"
         }),
         "repo": JSON.stringify({
           "id": 478747,
           "name": "ngriffin/rerum-aliquam-cum",
           "url": "https://github.com/ngriffin/rerum-aliquam-cum"
         }),
         "created_at": "2013-04-17 04:13:31"
       }
     ], {});
  },

  down: async (queryInterface, Sequelize) => {

     await queryInterface.bulkDelete('Events', null, {});
  }
};
