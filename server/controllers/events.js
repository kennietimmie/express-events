const Event = require('../models').Event;
const db = require('../models/index');


module.exports = {
	// Adding new events
	create(req, res) {
		return Event
			.create({
				id: parseInt(req.body.id),
				type: req.body.type,
				actor: JSON.parse(req.body.actor),
				repo: JSON.parse(req.body.repo),
				created_at: req.body.created_at
			}, {
				attributes: {
					exclude: ['createdAt', 'updatedAt'],
				}})
			.then(event => res.status(201).send(event))
			.catch(error => {
				if (error.name === "SequelizeValidationError" || "SequelizeUniqueConstraintError") {
					res.status(400).send(error.errors);
				}
				res.status(400).send({ mesage: "error occured" });
			});
	},

	// Returning all the event
	list(req, res) {
		return Event
			.findAll({
				attributes: {
					exclude: ['createdAt', 'updatedAt'],
				},
				order: [
					['id', 'ASC'],
				]
			})
			.then(events => {
				return res.status(200).send(events);
			})
			.catch(error => res.status(200).send(error));
	},

	// Returning the actor records ordered by the total number of events
	retrieve(req, res) {
		return Event.findAll({
			attributes: { exclude: ['createdAt', 'updatedAt'], },
			order: []
		})
			.then(actors => res.status(200).send(actors))
			.catch(error => res.status(400).send(error));
	},

	// Returning the event records filtered by the actor ID
	retrieveEventsByActorId(req, res) {
		return Event.findAll({
			where: {
				actor: {
					id: req.params.id
				}
			},
			attributes: { exclude: ['createdAt', 'updatedAt'], },
			order: [
				['id', 'ASC'],
			]
		})
			.then(events => {
				if (events) {
					return res.status(200).send(events);
				}
				return res.status(404).send({ message: 'Actor not found' });
			})
			.catch(error => res.status(400).send(error));
	},

	// Updating the avatar URL of the actor
	updateActor(req, res) {
		return Event.findOne({
			where: { actor: { id: req.body.id } }
		})
			.then(event => {

				// get at atleast one actor's event
				if (event) {

					// make sure we are not updating the actor.login
					if (event.actor.login == req.body.login) {
						// return json array of events by the actor
						return Event.update({
							actor: {
								id: parseInt(req.body.id),
								login: req.body.login,
								avatar_url: req.body.avatar_url
							}
						}, {
							where: { actor: { id: req.body.id, login: req.body.login } },
							validate: true,
							returning: ["id", "actor", "repo", "created_at"]
						})
							.then(([eventsCount, events]) => res.status(200).send(events))
							.catch(error => res.status(400).send({ mesage: 'Actor not found' }));
					}

					return res.status(400).send({ message: "only actor avatar_url is editable." });
				}
				return res.status(400).send({ mesage: 'Actor not found' })
			})
			.catch(error => res.status(400).send(error));
	},

	streak(req, res) {

	},

	// Erasing all the events
	erase(req, res) {
		return Event
			.destroy({
				where: {},
				truncate: true,
			})
			.then(() => res.status(200).send())
			.catch(error => res.status(400).send(error));
	}
}