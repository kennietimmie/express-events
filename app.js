const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');

// set up the express app
const app = express();

// log the requests to the console
app.use(logger('dev'));

// parse incoming requests data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// require our routes
require('./server/routes')(app);

module.exports = app;